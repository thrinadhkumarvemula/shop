"""shopping URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from knox import views as knox_views
from cart import serializersviews
from cart.views import RegisterAPI
from cart.views import LoginAPI


urlpatterns = [
    path('admin/', admin.site.urls),
    # product Rest-framework
    path("products/", serializersviews.TodoList.as_view()),
    path("products/<username>", serializersviews.TodoList.as_view()),
    path("products/<int:pk>", serializersviews.TodoDetails.as_view()),

    # ORDERS Rest-framework

    path("orders/", serializersviews.Order.as_view()),
    path("orders/<username>", serializersviews.Order.as_view()),
    path("orders/<int:pk>", serializersviews.OrderDetails.as_view()),

    # ORDERSITEMS Rest-framework

    path("ordersitems/", serializersviews.OrderitemsList.as_view()),
    path("ordersitems/<username>", serializersviews.OrderitemsList.as_view()),
    path("ordersitems/<int:pk>", serializersviews.OrderitemsDetails.as_view()),



    path('api/register/', RegisterAPI.as_view(), name='register'),
    path('api/login/', LoginAPI.as_view(), name='login'),
    path('api/logout/', knox_views.LogoutView.as_view(), name='logout'),
    path('api/logoutall/', knox_views.LogoutAllView.as_view(), name='logoutall')
]

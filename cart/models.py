from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import User


class Products(models.Model):
    Title = models.CharField(max_length=200, null=True)
    Description = models.CharField(max_length=300, null=True)
    image = models.FileField(upload_to='photos/', blank=True, null=True)
    Added_Date = models.DateField(auto_now_add=True)
    Updated_Date = models.DateField(auto_now=True)
    Price = models.FloatField()

    def __str__(self):
        return f"{self.Title} - {self.Description} - {self.image} - {self.Added_Date} - {self.Updated_Date} - {self.Price}"



class Orders(models.Model):
    payment_choices = (
        ('cash', 'Cash'),
        ('paytm', 'Paytm'),
        ('card', 'Card'),
    )

    status_choices=(
        ('new','New'),
        ('paid','Paid')

    )

    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    Total = models.IntegerField()
    Added_Date= models.DateTimeField(auto_now_add=True)
    Updated_Date = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=20,choices=status_choices)
    Mode_of_payments = models.CharField(max_length=30, choices=payment_choices)

    def __str__(self):
        return f"{self.Total} - {self.user_id} -{self.status}- {self.Added_Date} - {self.Updated_Date} - {self.Mode_of_payments}"


class OrdersItems(models.Model):
    order_id = models.ForeignKey(Orders, on_delete=models.CASCADE)
    product_id = models.ForeignKey(Products, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    price = models.FloatField()


    def __str__(self):
        return f"{self.order_id} - {self.product_id} -{self.quantity}- {self.price}"
from rest_framework import serializers

from .models import Products
from .models import Orders
from .models import OrdersItems



class Myserializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ('Title', 'Description', 'image','Added_Date', 'Updated_Date','Price')



class MyserializerOrders(serializers.ModelSerializer):
    class Meta:
        model = Orders
        fields = ('Total', 'user_id', 'Added_Date', 'Updated_Date','Mode_of_payments','status')




class MyserializerOrdersItems(serializers.ModelSerializer):
    class Meta:
        model = OrdersItems
        fields = ('quantity', 'order_id', 'product_id', 'price')



from rest_framework import serializers
from django.contrib.auth.models import User

# User Serializer
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')

# Register Serializer
class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(validated_data['username'], validated_data['email'], validated_data['password'])

        return user





from django.contrib.auth.models import User
from rest_framework.authentication import BasicAuthentication, TokenAuthentication

from cart.models import Products
from cart.models import Orders
from cart.models import OrdersItems
from cart.Serializer import Myserializer
from cart.Serializer import MyserializerOrders
from cart.Serializer import MyserializerOrdersItems
from rest_framework import generics
from rest_framework.permissions import IsAdminUser, IsAuthenticated, IsAuthenticatedOrReadOnly


class TodoList(generics.ListCreateAPIView):
    queryset = Products.objects.all()
    serializer_class = Myserializer
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]

class TodoDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Products.objects.all()
    serializer_class = Myserializer
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]



class Order(generics.ListCreateAPIView):
    queryset = Orders.objects.all()
    serializer_class = MyserializerOrders
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]

class OrderDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Orders.objects.all()
    serializer_class = MyserializerOrders
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]


class OrderitemsList(generics.ListCreateAPIView):
    queryset = OrdersItems.objects.all()
    serializer_class = MyserializerOrdersItems
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]

class OrderitemsDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Orders.objects.all()
    serializer_class = MyserializerOrdersItems
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]

